(function() {

  'use strict';

  angular.module('dashboardController', []).controller('DashboardController',
    function($scope, BrowserFactory, TemperaturaFactory, TokenFactory){

    var token = TokenFactory.get();

    $scope.titulo = 'Dashboard';
    $scope.browsers = new Array();
    $scope.temperaturas = new Array();

    var listaBrowsers = function(){
    BrowserFactory.listar(token).then(function(retorno){
        $scope.browsers = retorno;
      });
    };

    var listaTemperaturas = function(){
      TemperaturaFactory.listar(token).then(function(retorno){
        var datas = [];
        angular.forEach(retorno, function(data, key){
          data.id = key;
          datas.push(data);
        });
        for (var key = 0; key < datas.length; key++  ) {
              //console.log('Tamanho de key:', key, 'Cidade:', datas[key].name);
              console.log('ID:', datas[key].id, 'Cidade:', datas[key].name);
              //for (var dt = 0; dt <  datas[key].data.length; dt++ )  {
                  //console.log('Tamanho de data:', dt, 'Dados da data:', datas[dt].id);
              //}
        }
        $scope.temperaturas = retorno;
      });
    };
    listaBrowsers();
    listaTemperaturas();
  });

})();
