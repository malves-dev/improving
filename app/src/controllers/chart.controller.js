(function() {

  'use strict';

  angular.module('app', []).controller('ChartController', function($scope){
     // chart data source
     $scope.dataSource = {//
      "chart" : {
        "caption" : "Número de acessos por browser",
        "captionFontSize" : "30",
        // more chart properties - explained later
      },
      "data": [
        {
          "label" : "CornflowerBlue",
          "value" : "42"
        } //,more chart data
       ]
      }//
  });

})();
