(function(){

	'use strict';

	angular.module('app', []).factory('BrowserFactory', function($q, $http){
		var urlApi = "http://www.improving.com.br/api/test/hits-by-browser";
		return {
			listar : function(token) {
				var promessa = $q.defer();
				var token = {"token":"0a1b2c3d"};
				//API de browsers
				$http({
					withCredentials: false,
					method : 'post',
					url : urlApi,
					headers : {'Content-Type': 'text/json'},
					data :  token
				})
				.then(function(response) {
					promessa.resolve(response.data);
				})
				.catch(function(error) {
					return error;
				});
				return promessa.promise;
			}
		};
	});

})();
