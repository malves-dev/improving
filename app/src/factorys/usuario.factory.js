(function(){

	'use strict';

	angular.module('app', []).factory("UsuarioFactory", function($q, $http){
		var urlAPIUser = "http://www.improving.com.br/api/test/users";
		return {
			// API de cadastro de usuário
			inserir: function(usuario) {
				return $http.post(urlAPIUser, usuario);
			}
		};
	});

})();
