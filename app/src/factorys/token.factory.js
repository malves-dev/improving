(function(){

  'use strict';

  angular.module('app', []).factory("TokenFactory", function($scope){
      var token;

      var addToken = function(obj){
          token = obj;
      };

      var getToken = function(){
        return token;
      };

      return {
        add : addToken,
        get : getToken
      };
  });
})();
