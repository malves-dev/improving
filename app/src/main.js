
var app = angular.module('app', [
  'ngRoute',
  'usuarioController',
  'dashboardController'
  //'ngMessages',
  //'validation.match'
  // 'chartController',
  // 'ng-fusioncharts',
  //
]);
// Database Connexion
// require('./config/database.js')('mongodb://localhost/spa');

app.config(function($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl : 'views/partials/signup.html',
    controller  : 'UsuarioController'
  })
  .when('/dashboard', {
    templateUrl : 'views/partials/dashboard.html',
    controller  : 'DashboardController'
  })
  .otherwise({
    redirectTo : '/'}
  );
});
